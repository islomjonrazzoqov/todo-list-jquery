var selectedItem=null;
var todos=[
    {
        'id':1,
        'task':'Ertalab uyg`onaman',
        'isCompleted':false
    },
    {
        'id':2,
        'task':'Nonushta qilaman',
        'isCompleted':false
    },{
        'id':3,
        'task':'O`qishga boraman',
        'isCompleted':false
    }
];

$(document).ready(function(){
    showTodo();
    $(".button-task").click(function(){
        var inputTodo=$(".task-input");
        var inputValue=inputTodo.val().trim();
        var errorMessage=null;
        if(!inputValue){
            errorMessage="Task cannot be empty!";
            toastInfo("Error!",errorMessage,'error')
        }
        else{
            todos.forEach(function(todo){
                if(todo.task.toLowerCase()===inputValue.toLowerCase()){
                    errorMessage="Task already exists."
                    toastInfo("Information!",errorMessage,'info')
                }
                    
            })
            if(errorMessage!=="Task already exists."){
                if(selectedItem!==null){
                    todos.push({
                        'id':(selectedItem),
                        'task':inputValue,
                        'isCompleted':false
                    })
                    todos=todos.sort(function(a,b){return a.id-b.id});
                    selectedItem=null;
                    toastInfo('Success!','You changed the task','success')
                }
                else{
                    todos.push({
                        'id':(todos.length+1),
                        'task':inputValue,
                        'isCompleted':false
                    })
                    toastInfo('Success!','You have added a new task.','success')
                }
                inputTodo.val('');
            }
        }
        showTodo();
    })
})
function showTodo(){
    $(".checklist").html('');
    todos.map(function(item){
        $(".checklist").append(`<div class="checklist-item fs-4 fw-bold">
                                    <div class="task-name">
                                        <input id="${item.id}"  type="checkbox">
                                        <label class="${item.id}" for="${item.id}" onclick="completed(${item.id})">${item.task}</label>
                                    </div>
                                    <div class="action">
                                        <button class="btn btn-transparent text-primary" id="editBtn" title="Edit Task" onclick="editTask(${item.id})"><i class="fas fa-pencil-alt fa-2x"></i></i></button>
                                        <button class="btn btn-transparent text-danger" id="deleteBtn" title="Delete Task" onclick="deleteTask(${item.id})"><i class="fas fa-trash fa-2x"></i></button>
                                    </div>
                                </div>`)
                                if(item.isCompleted){
                                    $(`.${item.id}`).addClass("strike");
                                    $(`#${item.id}`).prop( "checked", true );
                                }
                                else{
                                    $(`.${item.id}`).removeClass("strike");
                                }
    })
}

function editTask(id){
    selectedItem=id;
    var findTask=todos.find(item=>item.id===id);
    $(".task-input").val(findTask.task); 
    var filterTodos=todos.filter(item=>item.id!==id);
    todos=filterTodos;
}

function deleteTask(id){
    var filterTodos=todos.filter(item=>item.id!=id);
    todos=filterTodos;
    showTodo();
}
function toastInfo(info,message,icon){
    $.toast({
        heading:info,
        text:message,
        showHideTransition: 'slide',
        icon: icon,
        position:'bottom-center',
        textColor:'white'
    });
}

function completed(id){
    var findTask=todos.find(item=>item.id===id);
    if(findTask['isCompleted'])
        findTask['isCompleted']=false;
    else findTask['isCompleted']=true;
    $(`.${id}`).toggleClass("strike")
}


